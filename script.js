$(document).ready(function(){
     
    // The dinosaurs walked the earth  
    Date.prototype.formatDate = function (format) {
        var date = this,
            day = date.getDate(),
            month = date.getMonth() + 1,
            year = date.getFullYear(),
            hours = date.getHours(),
            minutes = date.getMinutes(),
            seconds = date.getSeconds();
     
        if (!format) {
            format = "MM/dd/yyyy";
        }
     
        format = format.replace("MM", month.toString().replace(/^(\d)$/, '0$1'));
     
        if (format.indexOf("yyyy") > -1) {
            format = format.replace("yyyy", year.toString());
        } else if (format.indexOf("yy") > -1) {
            format = format.replace("yy", year.toString().substr(2, 2));
        }
     
        format = format.replace("dd", day.toString().replace(/^(\d)$/, '0$1'));
     
        if (format.indexOf("t") > -1) {
            if (hours > 11) {
                format = format.replace("t", "pm");
            } else {
                format = format.replace("t", "am");
            }
        }
     
        if (format.indexOf("HH") > -1) {
            format = format.replace("HH", hours.toString().replace(/^(\d)$/, '0$1'));
        }
     
        if (format.indexOf("hh") > -1) {
            if (hours > 12) {
                hours -= 12;
            }
     
            if (hours === 0) {
                hours = 12;
            }
            format = format.replace("hh", hours.toString().replace(/^(\d)$/, '0$1'));
        }
     
        if (format.indexOf("mm") > -1) {
            format = format.replace("mm", minutes.toString().replace(/^(\d)$/, '0$1'));
        }
     
        if (format.indexOf("ss") > -1) {
            format = format.replace("ss", seconds.toString().replace(/^(\d)$/, '0$1'));
        }
     
        return format;
    };

    // teleport

    var d = new Date();
    
   
    // Kill it with fire
    $('.answer_form_container').remove();
    
    // reverse post order - newest at bottom
    var $e = $('#posts');
    $('article.post')
        .each(function(i,t){
            $e.prepend(t);
        });
    
    // split text posts with imgs into bubbles
    $('article.text .post-content:has(img)').each(function(){
        var $post = $(this).removeClass('bubble')
            ,$p = $post.find('p').remove()
            ,bbstr = '<div class="bubble"></div>'
            ,$bb = null
            ;
        // loop <p>'s
        $p.each(function(){
            var $t = $(this)
                ,$img = $t.find('img')
                ;
            // look at content of <p>
            // has img
            if($img.length){
                // is txt bubble open?
                if($bb){
                    // finish prior bubble
                    $bb.appendTo($post);
                    $bb = null;
                }
                $('<div class="img-box"></div>')
                    .append($img)
                    .appendTo($post);
            }
            // no img
            else {
                // add to txt bubble
                if(!$bb) $bb = $(bbstr);
                $t.appendTo($bb);
            }
        });
        if($bb) $bb.appendTo($post);
    });

    $('.chat-status span').text(d.formatDate("dd/MM/yyyy hh:mm:ss t"));
    // scroll to page bottom on load
    $(window).load(function(){
        $("html, body").animate({ scrollTop: $(document).height() }, 1000, function(){
            // hide preloader
            $('.preloader').fadeOut('fast', function(){
                // show content
                $('#page').animate({
                    opacity: 1
                }, 800);
            });

        });
    });
});
